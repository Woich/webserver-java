package com.webserver;

import java.io.* ;
import java.net.* ;
import java.util.* ;

final class HttpRequest implements Runnable{

	final static String CRLF = "\r\n";
	Socket socket;
	// Constructor
	public HttpRequest(Socket socket) throws Exception {
	
		this.socket = socket;
	}
	
	@Override
	public void run() {
		try {
            processRequest();
        }
        catch (Exception e) {
            System.out.println(e);
        }
		
	}
	
	private void processRequest() throws Exception{
		//Inicializa as referencias para o input e output do Socket
		InputStreamReader inputStream = new InputStreamReader(socket.getInputStream());
		DataOutputStream outputStream = new DataOutputStream(socket.getOutputStream());
		
		//Inicializa os filtros de input
		BufferedReader bufferedReader = new BufferedReader(inputStream);
		
		//Obtem a linha de requeste do HTTP
		String linhaDaRequest = bufferedReader.readLine();
		
		//Imprime a linha
		System.out.println();
		System.out.println(linhaDaRequest);
		
		//Obtem e apresenta as linhas de cabeçalho
		String cabecalho = null;
		while((cabecalho = bufferedReader.readLine()).length() != 0){
			System.out.println(cabecalho);
		}
		
		//Obtem o nome do arquivo
		StringTokenizer tokens = new StringTokenizer(linhaDaRequest);
		tokens.nextToken();
		
		String nomeArquivo = tokens.nextToken();
		nomeArquivo = "." + nomeArquivo;
		
		//Abre o arquivo requisitado
		FileInputStream arquivoIS = null;
		
		boolean arquivoExiste = true;
		
		try{
			arquivoIS = new FileInputStream(nomeArquivo);
		
		}catch(FileNotFoundException e){
			arquivoExiste = false;
		}
		
		//Cria a mensagem de resposta
		String status = null;
		String tipoConteudo = null;
		String corpo = null;
		
		if(arquivoExiste){
			status = "HTTP/1.0 200 OK";
			tipoConteudo = "Content-type: " + contentType(nomeArquivo) + CRLF;
		}else{
			status = "HTTP/1.0 404 Not Found";
			tipoConteudo = "Content-type: text/html" + CRLF;
			corpo = "<HTML>" + "<HEAD><TITLE>Not Found</TITLE></HEAD>" +"<BODY>Not Found</BODY></HTML>";
		}
		
		//Realiza os envios
		outputStream.writeBytes(status);
		outputStream.writeBytes(tipoConteudo);
		outputStream.writeBytes(CRLF);
		
		if(arquivoExiste){
			sendBytes(arquivoIS, outputStream);
			arquivoIS.close();
		}else{
			outputStream.writeBytes(corpo);
		}
		
		//Fecha as streams e o Socket
		outputStream.close();
		bufferedReader.close();
		socket.close();
		
	}
	
	private static void sendBytes(FileInputStream arquivoIS, OutputStream outputStream) throws Exception{
		
		//Constroi o buffer de 1K para os bytes a caminho do Socket
		byte[] buffer = new byte[1024];
		int bytes = 0;
		
		//Copia o arquivo da request para o Socket
		while((bytes = arquivoIS.read(buffer)) != -1){
			outputStream.write(buffer, 0, bytes);
		}
		
	}
	
	private static String contentType(String nomeArquivo){
		
		if(nomeArquivo.endsWith(".htm") || nomeArquivo.endsWith(".html")){
			return "text/html";
		}
		
		if(nomeArquivo.endsWith(".gif")) {
            return "image/gif";
        }
        
        if(nomeArquivo.endsWith(".jpg") || nomeArquivo.endsWith(".jpeg")) {
            return "image/jpeg";
        }
        
        return "application/octet-stream";
	}
}
