package com.webserver;

import java.io.* ;
import java.net.* ;
import java.util.* ;

/**
 * @author Pedro Henrique Woiciechovski
 */
public final class WebServer {

	/**
     * @param args the command line arguments
     */
	public static void main(String[] args) throws Exception{
		
		// Set the port number.
		int porta = 6789;
		
		ServerSocket SCK_inicial = new ServerSocket(porta);
		
		while(true){
			
			Socket SCK_conexao = SCK_inicial.accept();
			
			//Cria um objeto para tratar o HTTP request
			HttpRequest request = new HttpRequest(SCK_conexao);
			
			//Cria uma nova tarefa para processar o request
			Thread tarefa = new Thread(request);
			
			//Inicia a tarefa
			tarefa.start();
		}

	}

}
